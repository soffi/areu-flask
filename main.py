import math
import ROOT as R
from array import array

from flask import Flask, render_template, url_for, request
app = Flask(__name__)
@app.route('/', methods=['GET', 'POST'])

def index():
  covid_result = False
  if request.method == 'POST':
    form = request.form
    covid_result = calculate_covid(form)
  return render_template('index.html', covid_result=covid_result)

def makeSexNum(str_sex):
  if str_sex=='f':
    return 0
  elif str_sex=='m':
    return 1
  elif str_sex=='n':
    return 2

def makeDSSeverityNum(str_cod):
    if str_cod=='b':
        return 0
    elif str_cod=='v':
        return 1
    elif str_cod=='vg':
        return 2
    elif str_cod=='g':
        return 3
    elif str_cod=='r':
        return 4
    elif str_cod=='rp':
        return 5

def evaluateMVA(age,sex,sev,hour):
  reader = R.TMVA.Reader( "!Color:!Silent" )
  var0 = array('f',[0])
  var1 = array('f',[0])
  var2 = array('f',[0])
  var3 = array('f',[0])
  reader.AddVariable( "VL_PAT_AGE", var0 )
  reader.AddVariable( "AC_PAT_SEX_NUM",  var1 )
  reader.AddVariable( "DS_SEVERITY_LEVEL_NUM", var2 )
  reader.AddVariable( "HOUR_EMERG_OPEN",       var3 )
  reader.BookMVA( "DNN_MLP", "TMVAClassification_101021_MLP.weights.xml" )

  var0[0] =age
  var1[0] =sex
  var2[0] =sev
  var3[0] =hour
  DNN_MLP = reader.EvaluateMVA("DNN_MLP")
  print(DNN_MLP)
  return DNN_MLP

def calculate_covid(form):
  str_sex = request.form['sex']
  sex=makeSexNum(str_sex)
  age = int(request.form['age'])
  hour = int(request.form['hour'])
  str_ds_sev_lev = request.form['ds_sev_lev']
  ds_sev_lev=makeDSSeverityNum(str_ds_sev_lev)
  result = evaluateMVA(age,sex,ds_sev_lev,hour)
  return (result)
if __name__ == "__main__":
  app.run(debug=True)
