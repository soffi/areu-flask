import math
import ROOT as R
from array import array
var0 = array('f',[0])
var1 = array('f',[0])
var2 = array('f',[0])
var3 = array('f',[0])
var4 = array('f',[0])

reader = R.TMVA.Reader( "!Color:!Silent" )
reader.AddVariable( "VL_PAT_AGE", var0 )
reader.AddVariable( "AC_PAT_SEX_NUM",  var1 )
reader.AddVariable( "DS_SEVERITY_LEVEL_NUM", var2 )
reader.AddVariable( "HOUR_EMERG_OPEN",       var3 )
reader.AddVariable( "DS_REASON_DTL_NUM", var4 )
reader.BookMVA( "MLP", "weights/TMVAClassification_MLP_5variables.weights.xml")

from flask import Flask, render_template, url_for, request
app = Flask(__name__)
@app.route('/', methods=['GET', 'POST'])

def index():
  covid_result = False
  if request.method == 'POST':
    form = request.form
    covid_result = calculate_covid(form)
  return render_template('index_PM.html', covid_result=covid_result)

def makeSexNum(str_sex):
  if str_sex=='f':
    return 0
  elif str_sex=='m':
    return 1
  elif str_sex=='n':
    return 2

def makeDSSeverityNum(str_cod):
    if str_cod=='b':
        return 0
    elif str_cod=='v':
        return 1
    elif str_cod=='vg':
        return 2
    elif str_cod=='g':
        return 3
    elif str_cod=='r':
        return 4
    elif str_cod=='rp':
        return 5

def makeDSReasonDTLNum(str_ds_reason):
  if str_ds_reason=='i':
    return 0
  elif str_ds_reason=='r':
    return 1
  elif str_ds_reason=='a':
    return 2

  
def evaluateMVA(age,sex,sev,hour,reason):
  var0[0] =age
  var1[0] =sex
  var2[0] =sev
  var3[0] =hour
  var4[0] =reason
  score = reader.EvaluateMVA("MLP")
#  score = 1
  print({"score":score})
  return score

def calculate_covid(form):
  str_sex = request.form['sex']
  sex=makeSexNum(str_sex)
  age = int(request.form['age'])
  hour = int(request.form['hour'])
  str_ds_sev_lev = request.form['ds_sev_lev']
  ds_sev_lev=makeDSSeverityNum(str_ds_sev_lev)
  str_ds_reason=request.form['ds_reason']
  ds_reason=makeDSReasonDTLNum(str_ds_reason)
  print({"age":age,"sex":sex,"ds_sev_lev":ds_sev_lev,"hour":hour,"ds_reason":ds_reason})
  result = evaluateMVA(age,sex,ds_sev_lev,hour,ds_reason)
  return (result)

if __name__ == "__main__":
  app.run(host="0.0.0.0", debug=True, port=80)
